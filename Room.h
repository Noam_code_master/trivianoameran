#pragma once
#include <iostream>
#include "User.h"
#include "Helper.h"
#include <winsock2.h>
#include <windows.h>
using namespace std;

class User;
class Room {
public:
	Room(int, User*, string, int, int, int);
	string getUsersAsString(vector<User*> userList, User* excludeUser);
	string getUsersListMessage();
	void sendMessage(User* excludeUser, string message);
	void sendMessage(string message);
	bool joinRoom(User*);
	void leaveRoom(User*);
	int closeRoom(User*);
	vector<User*> getUsers();
	//string getUsersListMessage();
	// get functions
	int getQuestionsNo();
	int getId();
	string getName();
	int getMaxUsers() { return _maxUsers; };
	User* getAdmin() { return _admin; };

private:

	/*string getUsersAsString(vector<User*>, User*);
	void sendMessage(string);
	void sendMessage(User*, string);*/

	vector<User*> _users;
	User* _admin;
	int _maxUsers;
	int _questionTime;
	int _questionNo;
	string _name;
	int _id;
};