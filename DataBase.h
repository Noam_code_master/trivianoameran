#pragma once
#include <winsock2.h>
#include <windows.h>
#include <string>
#include <sstream>
#include <time.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
//#include "Question.h"
#include "sqlite3.h"
#include "RecievedMessage.h"
#include <time.h>
#define NUMBER_OF_QUESTIONS 11
using namespace std;
class Question;
class DataBase {
public:
	DataBase() ;
	~DataBase() ;
	
	bool isUserExists(string); // done
	bool addNewUser(string username, string password, string email); //done
	bool isUserAndPassMatch(string username, string password); // done
	
	vector<Question*> initQuestions(int number_of_questions);
	vector<string> getBestScores(); // Not in checkpoint 2 !!!
	vector<string> getPersonalStatus(string); // Not in checkpoint 2 !!!

	int insertNewGame(); // done

	bool updateGameStatus(int game_id); // done
	bool addAnswerToPlayer(int game_id, string username, int question_id, string player_answer, bool is_correct, int answer_time); // done
	static int* _questionsIndexes;
	static int _number_of_questions;
	static vector<Question*> _vector;
private:

	sqlite3 *_db; // pointer to db created in constructor
	char *zErrMsg;
	bool static _is_Users_Passowrds;
	bool static _has_succseeded; /* this callback function isn't supposed to print anything, and
								 this boolean argument becomes false if it does print*/
	int static _curr_new_game_id;

	// Callback functions:

	/* Specific callback and arguments for testing usernames and passwords */
	int static callback_Users_Passwords(void *NotUsed, int argc, char **argv, char ** azColName);

	/* Game ID callback */
	int static callbackGameID(void *NotUsed, int argc, char **argv, char ** azColName);
};