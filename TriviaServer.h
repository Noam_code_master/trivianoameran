#pragma once
#include <winsock2.h>
#include <windows.h>
#include <string>
#include "RecievedMessage.h"
#include "DataBase.h"
#include <map>
#include "User.h"
#include "Validator.h"
#include "Room.h"
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <thread>
#include <mutex>
#include <queue>
#include "Helper.h"
using namespace std;

class TriviaServer {
private:
	SOCKET _socket;
	map<SOCKET, User*> _connectedUsers;
	DataBase _db;
	map<int, Room*> _roomsList;
	
	mutex _mtxRecievedMessages;
	queue<RecievedMessage*> _queRcvMessages;

	int _roomIdSequence;
	sockaddr_in _server, _client;
	int _c;

	void bindAndListen();
	//void accept2();
	void clientHandler(SOCKET);
	//void safeDeleteUser(RecievedMessage*);
	User* handleSignin(RecievedMessage*);
	bool handleSignup(RecievedMessage*);
	void handleSignout(RecievedMessage*);

	void handleStartGame(RecievedMessage*);
	void handleLeaveGame(RecievedMessage* message);
	
	void handlePlayerAnswer(RecievedMessage*);

	void handleGetUsersInRoom(RecievedMessage*);
	bool handleCreateRoom(RecievedMessage*);
	bool handleCloseRoom(RecievedMessage*);
	bool handleJoinRoom(RecievedMessage*);
	bool handleLeaveRoom(RecievedMessage*);
	void handleGetRooms(RecievedMessage*);

	/*void handleGetBestScores(RecievedMessage*);
	void handleGetPersonalStatus(RecievedMessage*);

	void handleRecievedMessage();
	void addRecievedMessage(RecievedMessage*);
	RecievedMessage* buildRecievedMessage(SOCKET, int);*/

	User* getUserByName(string);
	//User* getUserBySocket(SOCKET);
	Room* getRoomById(int a) {
		return NULL;
	};


public:
	TriviaServer();
	//~TriviaServer();
	void server();
};