#include "Question.h"
#include <time.h>

Question::Question(int id, string question, string correct_answer, string answer2, string answer3, string answer4) {
	_correctAnswerIndex = rand() % 4; // generating the correct question index
	_answers[_correctAnswerIndex] = correct_answer;
	vector<string> wrongQuestions;
	wrongQuestions.push_back(answer2);
	wrongQuestions.push_back(answer3);
	wrongQuestions.push_back(answer4);
	for (int i = 0; i < 4; i++) {
		if (i == _correctAnswerIndex) {
			continue;
		}
		int currIndex = rand() % wrongQuestions.size();//We already did srand(time(NULL)) in initQuestions in DataBase and this constructor is called only
														//afterwards...
		_answers[i] = wrongQuestions[currIndex];
		wrongQuestions.erase(wrongQuestions.begin() + currIndex);
	}
	_question = question;
	_id = id;
}

string Question::getQuestion() {
	return _question;
}

string * Question::getAnswers() {
	return _answers;
}

int Question::getCorrectAnswerIndex() {
	return _correctAnswerIndex;
}

int Question::getId() {
	return _id;
}
