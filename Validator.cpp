#include "Validator.h"
#include <ctype.h>
bool Validator::isPasswordValid(string password) {
	if (password.length() < 4) {
		return false;
	}
	if (password.find(' ') != string::npos) {//If there are spaces in the password:
		return false;
	}
	bool found_digit = false;
	bool found_lower_letter = false;
	bool found_upper_letter = false;
	for (int i = 0; i < password.length(); i++) {
		if (isdigit(password.at(i))) {
			found_digit = true;
		}
		if (islower(password.at(i))) {
			found_lower_letter = true;
		}
		if (isupper(password.at(i))) {
			found_upper_letter = true;
		}
	}
	return found_digit && found_lower_letter && found_upper_letter;
}
bool Validator::isUsernameValid(string username) {
	if (username == "") {
		return false;
	}
	if (!(islower(username.at(0)) || isupper(username.at(0)))) {//If it doesn't begin with a letter:
		return false;
	}
	if (username.find(' ') != string::npos) {//If there are spaces in the password:
		return false;
	}
	return true;
}