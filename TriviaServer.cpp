#include "TriviaServer.h"
#define MAX_SOCKETS 100
#define END_CONNECTION 299
#define SIGN_IN 200
#define SIGN_OUT 201
#define SIGN_UP 203
#define ASK_ROOMS 205
#define GET_USERS 207
#define JOIN_ROOM 209
#define LEAVE_ROOM 211
#define CREATE_ROOM 213
#define CLOSE_ROOM 215
#define BEGIN_GAME 217
#define ANSWER_OF_CLIENT_ON_QUESTION 219
#define LEAVE_GAME 222
#define BEST_SCORES 223
#define PERSONAL_STATUS 225

#define MAX_LEN 1000
void TriviaServer::handleGetUsersInRoom(RecievedMessage* message) {
	message->getUser()->send("2071012345678903abs");
}
void TriviaServer::handlePlayerAnswer(RecievedMessage* message) {
	if (message->getUser() == NULL) {
		return;
	}
	if (message->getUser()->getGame() != nullptr) {
		bool result = message->getUser()->getGame()->handleAnswerFromUser(message->getUser(),
			message->getValues()[0].at(0) - '0', (message->getValues()[0].at(1) - '0') * 10 + message->getValues()[0].at(2) - '0');
	}
}
User* TriviaServer::handleSignin(RecievedMessage* message) {
	string send_to_client;
	if (!_db.isUserAndPassMatch(message->getValues()[0], message->getValues()[1])) {
		send_to_client = "1021";//Wrong details.
		send(message->getSock(), ((string)"1021").c_str(), 4, 0);
		return NULL;//User and password do not match.
	}
	User* user = getUserByName(message->getValues()[0]);
	if (user != NULL) {
		send(message->getSock(), ((string)"1022").c_str(), 4, 0);
		return user;//User connected
	}
	user = new User(message->getValues()[0], message->getSock());
	_connectedUsers.insert(pair<SOCKET, User*>(message->getSock(), user));
	send(message->getSock(), ((string)"1020").c_str(), 4, 0);
	return user;//User and password do not match.
}

bool TriviaServer::handleSignup(RecievedMessage* message) {
	string send_to_client;
	if (!Validator::isUsernameValid(message->getValues()[0])) {//If password is not valid:
	send(message->getSock(), ((string)"1043").c_str(), 4, 0);
	return false;
	}
	if (!Validator::isPasswordValid(message->getValues()[1])) {//If password is not valid:
	send(message->getSock(), ((string)"1041").c_str(), 4, 0);
	return false;
	}
	if (_db.isUserExists(message->getValues()[0])) {
	send(message->getSock(), ((string)"1042").c_str(), 4, 0);
	return false;
	}
	if (!_db.addNewUser(message->getValues()[0], message->getValues()[1], message->getValues()[2])) {
	send(message->getSock(), ((string)"1044").c_str(), 4, 0);
	return false;
	}
	send(message->getSock(), ((string)"1040").c_str(), 4, 0);
	return true;
}
void TriviaServer::handleSignout(RecievedMessage* message) {
	if (message->getUser() != NULL) {
		_connectedUsers.erase(message->getUser()->getSocket());
	}
	handleCloseRoom(message);
	handleLeaveRoom(message);
	handleLeaveGame(message);
}
void TriviaServer::handleLeaveGame(RecievedMessage* message) {
	if (message->getUser()->leaveGame()) {
		delete message->getUser()->getGame();
	}
}
bool TriviaServer::handleCreateRoom(RecievedMessage* message) {
	if (message->getUser() == NULL) {
		return false;
	}
	bool succes = message->getUser()->createRoom(_roomIdSequence + 1, message->getValues()[0], message->getValues()[1].at(0) - '0',
		(message->getValues()[3].at(0) - '0') * 10 + message->getValues()[3].at(1) - '0', (message->getValues()[2].at(0) - '0') * 10 + message->getValues()[2].at(1) - '0');
	if (succes) {
		_roomIdSequence++;
		_roomsList.insert(pair<int, Room*>(_roomIdSequence, message->getUser()->getRoom()));
		Helper::sendData(message->getSock(), "1140");
		return true;
	}
	else {
		Helper::sendData(message->getSock(), "1141");
		return false;
	}
}
bool TriviaServer::handleCloseRoom(RecievedMessage* message) {
	if (message->getUser() == NULL) {
		return false;
	}
	if (message->getUser()->getRoom() == NULL) {
		return false;
	}
	int result = message->getUser()->closeRoom();
	if (result != -1) {
		_roomsList.erase(result);
	}
}
bool TriviaServer::handleJoinRoom(RecievedMessage* message) {
	if (message->getUser() == NULL) {
		return false;
	}
	int roomId = atoi(message->getValues()[0].c_str());
	Room* room = getRoomById(roomId);
	if (room == NULL) {
		Helper::sendData(message->getSock(), "1100");
		return false;
	}
	else {
		return message->getUser()->joinRoom(room);
	}
}
bool TriviaServer::handleLeaveRoom(RecievedMessage* message) {
	if (message->getUser() == NULL) {
		return false;
	}
	if (message->getUser()->getRoom() == NULL) {
		return false;
	}
	message->getUser()->leaveRoom();
	return true;
}
void TriviaServer::handleGetRooms(RecievedMessage* message) {
	Helper::sendData(message->getSock(), "1060001000101a");//this is a const just for the meanwhile...
}

User* TriviaServer::getUserByName(string username) {
	for (std::map<SOCKET, User*>::iterator it = _connectedUsers.begin(); it != _connectedUsers.end(); it++) {
		if (it->second == NULL) {
			continue;
		}
		if (it->second->getUsername() == username) {
			return it->second;
		}
	}
	return NULL;
}
void TriviaServer::handleStartGame(RecievedMessage* message) {
	if (message->getUser() == NULL) {
		return;
	}
	Game* game = new Game(message->getUser()->getRoom()->getUsers(), message->getUser()->getRoom()->getQuestionsNo(), _db);
	for (std::map<int, Room*>::iterator it = _roomsList.begin(); it != _roomsList.end(); it++) {
		if (it->second == message->getUser()->getRoom()) {
			_roomsList.erase(it->first);
		}
	}
	game->sendFirstQuestion();
}

TriviaServer::TriviaServer() {
	DataBase _db();
	WSADATA wsa;
	SOCKET new_socket;
	int c;
	char *message;

	printf("\nInitialising Winsock...");
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		printf("Failed. Error Code : %d", WSAGetLastError());
		return;
	}

	printf("Initialised.\n");

	//Create a socket
	if ((_socket = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		printf("Could not create socket : %d", WSAGetLastError());
	}

	printf("Socket created.\n");
}
void TriviaServer::server() {
	SOCKET clientSock;
	bindAndListen();
	while (true) {
		if ((clientSock = accept(_socket, (struct sockaddr *)&_client, &_c)) != INVALID_SOCKET)
		{
			puts("Connection accepted");

			//Reply to the client
			thread connection(&TriviaServer::clientHandler, this, clientSock);
			connection.detach();
		}
	}
}

void TriviaServer::bindAndListen() {
	//Prepare the sockaddr_in structure
	_server.sin_family = AF_INET;
	_server.sin_addr.s_addr = INADDR_ANY;
	_server.sin_port = htons(8820);

	//Bind
	unsigned int result = ::bind(_socket, (struct sockaddr *)&_server, sizeof(_server));
	if (result == SOCKET_ERROR) {
		cout << "Error #" << WSAGetLastError() << endl;
	}

	puts("Bind done");

	//Listen to incoming connections
	listen(_socket, MAX_SOCKETS);

	//Accept and incoming connection
	puts("Waiting for incoming connections...");

	_c = sizeof(struct sockaddr_in);
}

void TriviaServer::clientHandler(SOCKET clientSock) {
	User* curr_user = NULL;
	string message = "";
	int messageCode = -1;
	while (messageCode != 0 && messageCode != END_CONNECTION) {
		if (messageCode == SIGN_OUT) {
			_connectedUsers[clientSock] = NULL;
			curr_user = NULL;
		}
		if (messageCode == SIGN_IN) {
			message = Helper::getStringPartFromSocket(clientSock, MAX_LEN);
			int length_username = (message.at(0) - '0') * 10 + message.at(1) - '0';
			string username = message.substr(2, length_username);
			int length_password = (message.at(length_username + 2) - '0') * 10 + message.at(length_username + 3) - '0';
			string password = message.substr(length_username + 4, length_password);
			vector<string> values;
			values.push_back(username);
			values.push_back(password);
			RecievedMessage recievedMessage(clientSock, messageCode, values);
			curr_user = handleSignin(&recievedMessage);
			if (curr_user != NULL) {
				_connectedUsers.insert(pair<SOCKET, User*>(clientSock, curr_user));
			}
		}
		if (messageCode == SIGN_UP) {
			message = Helper::getStringPartFromSocket(clientSock, MAX_LEN);
			int length_username = (message.at(0) - '0') * 10 + message.at(1) - '0';
			string username = message.substr(2, length_username);
			int length_password = (message.at(length_username + 2) - '0') * 10 + message.at(length_username + 3) - '0';
			string password = message.substr(length_username + 4, length_password);
			int length_email = (message.at(4 + length_username + length_password) - '0') * 10 + message.at(5 + length_username + length_password) - '0';
			string email = message.substr(length_username + length_password + 6, length_email);
			vector<string> values;
			values.push_back(username);
			values.push_back(password);
			values.push_back(email);
			RecievedMessage recievedMessage(clientSock, messageCode, values);
			bool success = handleSignup(&recievedMessage);
		}
		if (messageCode == ASK_ROOMS) {
			RecievedMessage *recievedMessage = new RecievedMessage(clientSock, messageCode);
			handleGetRooms(recievedMessage);
		}

		if (messageCode == CREATE_ROOM) {
			message = Helper::getStringPartFromSocket(clientSock, MAX_LEN);

			int name_length = (message.at(0) - '0') * 10 + message.at(1) - '0';
			string name = message.substr(2, name_length);
			string num_players = message.substr(2 + name_length, 1);
			string num_questions = message.substr(3 + name_length, 2);
			string time_answer_questions = message.substr(5 + name_length, 2);

			vector<string> values;
			values.push_back(name);
			values.push_back(num_players);
			values.push_back(num_questions);
			values.push_back(time_answer_questions);
			RecievedMessage *messageRecieved = new RecievedMessage(clientSock, messageCode, values);
			if (curr_user != NULL) {
				messageRecieved->setUser(curr_user);
			}
			bool success = handleCreateRoom(messageRecieved);
		}
		if (messageCode == CLOSE_ROOM) {
			RecievedMessage *messageRecieved = new RecievedMessage(clientSock, messageCode);
			messageRecieved->setUser(curr_user);
			handleCloseRoom(messageRecieved);
		}
		if (messageCode == JOIN_ROOM) {
			RecievedMessage * messageRecieved = new RecievedMessage(clientSock, messageCode);
			messageRecieved->setUser(curr_user);
			handleJoinRoom(messageRecieved);
		}
		if (messageCode == GET_USERS) {
			message = Helper::getStringPartFromSocket(clientSock, MAX_LEN);
			vector<string> values;
			values.push_back(message);
			RecievedMessage* messageRecieved = new RecievedMessage(clientSock, messageCode, values);
			handleGetUsersInRoom(messageRecieved);
		}
		if (messageCode == LEAVE_ROOM) {
			RecievedMessage* messageRecieved = new RecievedMessage(clientSock, messageCode);
			handleLeaveRoom(messageRecieved);
		}
		if (messageCode == BEGIN_GAME) {
			RecievedMessage *messageRecieved = new RecievedMessage(clientSock, messageCode);
			messageRecieved->setUser(curr_user);
			handleStartGame(messageRecieved);
		}
		if (messageCode == ANSWER_OF_CLIENT_ON_QUESTION) {
			message = Helper::getStringPartFromSocket(clientSock, MAX_LEN);
			vector<string> values;
			values.push_back(message);
			RecievedMessage *messageRecieved = new RecievedMessage(clientSock, messageCode, values);
			messageRecieved->setUser(curr_user);
			handlePlayerAnswer(messageRecieved);
		}
		if (messageCode == LEAVE_GAME) {

		}
		if (messageCode == BEST_SCORES) {

		}
		if (messageCode == PERSONAL_STATUS) {

		}
		messageCode = Helper::getMessageTypeCode(clientSock);
		cout << messageCode << endl;
	}
	curr_user->send("299");
}