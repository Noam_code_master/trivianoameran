#include "DataBase.h"
#define MAX_LEN 1000
#pragma warning(disable : 4996)
using namespace std;
class Question;
bool DataBase::_is_Users_Passowrds = false;
bool DataBase::_has_succseeded = false;
int DataBase::_curr_new_game_id = 0;

int* DataBase::_questionsIndexes = NULL;
int DataBase::_number_of_questions = 0;
vector<Question*> DataBase::_vector;
static int callback(void *data, int argc, char **argv, char **azColName) {
	int i;
	fprintf(stderr, "%s: ", (const char*)data);
	for (i = 0; i<argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}

DataBase::DataBase(){
	int rc;
	char *sql;
	const char* data = "Callback function called";

	/* Open database */
	rc = sqlite3_open("trivia.db", &_db);
	if (rc) {
		fprintf(stderr, "Can't open database: %s\n", sqlite3_errmsg(_db));
		return;
	}
	else {
		fprintf(stderr, "Opened database successfully\n");
	}
}

DataBase::~DataBase(){
	/* Closing database */
	sqlite3_close(_db);
}

bool DataBase::isUserExists(string username){
	int rc;
	char *sql;
	const char* data = "Callback function called";
	_is_Users_Passowrds = false;
	string temp = (char*)("SELECT * FROM t_users WHERE username = \'" + username + "\'").c_str();
	sql = (char*)temp.c_str();

	/* Execute SQL statement */
	rc = sqlite3_exec(_db, sql, DataBase::callback_Users_Passwords, (void*)data, &zErrMsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
	}
	else {
		fprintf(stdout, "Operation done successfully\n");
	}
	return  _is_Users_Passowrds;
}

bool DataBase::isUserAndPassMatch(string username, string password){
	int rc;
	char *sql;
	const char* data = "Callback function called";
	_is_Users_Passowrds = false;
	/* Create SQL statement */
	string temp = "SELECT * FROM t_users WHERE username = \'" + username + "\' and password = \'" + password + "\'";
	sql = (char*)temp.c_str();

	/* Execute SQL statement */
	rc = sqlite3_exec(_db, sql, DataBase::callback_Users_Passwords, (void*)data, &zErrMsg);
	if (rc != SQLITE_OK) {
		fprintf(stderr, "SQL error: %s\n", zErrMsg);
	}
	else {
		fprintf(stdout, "Operation done successfully\n");
	}

	return _is_Users_Passowrds;
}

bool DataBase::addNewUser(string username, string password, string email){
	int rc = 0; 
	if (this->isUserExists(username)){
		return false; // if the new user already exists...
	}
	else{
		char *sql;

		string temp = "insert into t_users values(\'" + username + "\', \'" + password + "\', \'" + email  + "\');";
		sql = (char*)temp.c_str();
		/* Executing */

		/* Execute SQL statement */
		rc = sqlite3_exec(_db, sql, callback, 0, &zErrMsg);
		if (rc != SQLITE_OK) {
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
		}
		else {
			fprintf(stdout, "Records created successfully\n");
		}
	}
	return rc == 0;
}

int DataBase::callback_Users_Passwords(void * NotUsed, int argc, char **argv, char **azColName){
	/* Both the conditions related to this callback function check conditions - argv[0] is 0 or 1 */
	_is_Users_Passowrds =  (argc >= 1);//If it's 3 then we found a match!!!
	return 0;
}

/*int DataBase::callback(void * NotUsed, int argc, char **argv, char **azColName){
	/*
		This callback is called when the command isn't supposed to do anything,
		and the output should be empty.
	
	cout << "!!" << std::endl;
	if (argv[0] == ""){
		_has_succseeded = true;
	}
	else{
		_has_succseeded = false;
	}
	for (int i = 0; i < argc; i++){
		cout << argv[i];
	}
	return 0;
}*/

int DataBase::callbackGameID(void *NotUsed, int argc, char **argv, char ** azColName){
	_curr_new_game_id = atoi(argv[0]); // the game ID is in argv after the command
	return 0;
}

int DataBase::insertNewGame(){
	/* We are creating a new game */
	stringstream cmd;

	/* Creating instructions for database */
	
	/* Calculating our time - Trobishi this is how you create the time in the necessary format - use it after you are sure */
	time_t now = time(0);
	struct tm  tstruct;
	char       buf[80];
	tstruct = *localtime(&now);
	strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);
	
	/* Compiling and executing command , we create the game */
	cmd << "insert into t_games(status, start_time, end_time) values (0, NOW, NULL);";
	sqlite3_exec(_db, cmd.str().c_str(), callback, 0, &zErrMsg);

	/* Now we search the table to find the game we just created and return it's id */
	
	cmd = stringstream(); // deleting the previous command
	cmd << "select game_id from t_games where status = 0 and time_start = \"now\";"; // order to search the new game

	sqlite3_exec(_db, cmd.str().c_str(), DataBase::callbackGameID, 0, &zErrMsg); // This callback will insert data in a private argument

	return _curr_new_game_id;
}

bool DataBase::updateGameStatus(int game_id){
	/* Changing both time and status of the specific game */

	stringstream cmd;
	cmd << "update t_games set status = 1, end_time = \"now\" where id = " << game_id << ";";

	sqlite3_exec(_db, cmd.str().c_str(), callback, 0, &zErrMsg); // This callback will insert data in a private argument
	
	return _has_succseeded;
}

bool DataBase::addAnswerToPlayer(int game_id, string username, int question_id, string player_answer, bool is_correct, int answer_time){
	/* We need to the username */

	if (!this->isUserExists(username)){
		return false; // if the username doesn't exist
	}
	else{
		stringstream cmd;
		cmd << "insert into t_players_answers values(" << game_id << ", " << username << ", " << question_id << ", " << player_answer << ", " << is_correct << ", " << answer_time << ");";

		sqlite3_exec(_db, cmd.str().c_str(), callback, 0, &zErrMsg);
		return _has_succseeded;
	}
}
int callbackInitQuestions(void *data, int argc, char **argv, char **azColName) {
	for (int i = 0; i < DataBase::_number_of_questions; i++) {
		if (DataBase::_questionsIndexes[i] == atoi(argv[0])) {
			DataBase::_vector.push_back(new Question(atoi(argv[0]), argv[1], argv[2], argv[3], argv[4], argv[5]));
		}
	}
	return 0;
}

vector<Question*> DataBase::initQuestions(int number_of_questions) {
	vector<Question*> vector;
	_vector = vector;
	_questionsIndexes = new int[number_of_questions];
	_number_of_questions = number_of_questions;
	srand(time(NULL));
	for (int i = 0; i < number_of_questions; i++) {
		_questionsIndexes[i] = (rand() % NUMBER_OF_QUESTIONS) + 1;
	}
	stringstream cmd;
	cmd << "SELECT * FROM t_questions;";

	sqlite3_exec(_db, cmd.str().c_str(), callbackInitQuestions, 0, &zErrMsg);
	delete _questionsIndexes;
	return _vector;
}
vector<string> DataBase::getBestScores() {
	vector<string> vector;
	return vector;
}
vector<string> DataBase::getPersonalStatus(string) {
	vector<string> vector;
	return vector;
}