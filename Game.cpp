#include "Game.h"
using namespace std;
Game::Game(const vector<User*> & users, int questionNo, DataBase& db) : _db(db) {
	for (int i = 0; i < users.size(); i++) {
		users[i]->setGame(this);
		_players.push_back(users[i]);
	}
	_question_no = questionNo;
	_db = db;
	int id = _db.insertNewGame();
	if (id == -1) {
		return;
	}


	_questions = _db.initQuestions(questionNo);
	for (int i = 0; i < users.size(); i++) {
		_results.insert(pair<string,int>(users[i]->getUsername(), 0));
	}
	_currQuestionIndex = 0;
	_currentTurnAnswers = 0;
}

Game::~Game() {
	
}

void Game::sendQuestionToAllUsers() {
	string message = "118";
	if (_currQuestionIndex == _questions.size()) {
		_currQuestionIndex = 0;
	}
	Question* question = _questions[_currQuestionIndex];
	string q = question->getQuestion();
	message += q.length() / 100 + '0';
	message += (q.length() % 100) / 10 + '0';
	message += q.length() % 10 + '0';
	message += q;
	for (int i = 0; i < 4; i++) {
		string curr_answer = question->getAnswers()[i];
		message += curr_answer.length() / 100 + '0';
		message += (curr_answer.length() % 100) / 10 + '0';
		message += curr_answer.length() % 10 + '0';
		message += curr_answer;
	}
	for (int i = 0; i < _players.size(); i++) {
		_players[i]->send(message);
	}
}
void Game::handleFinishGame() {
	/* Sending the message to all of the players */
	_db.updateGameStatus(1);
	string message = "121";
	message += _players.size() + '0';
	for (int i = 0; i < _players.size(); i++) {
		message += _players[i]->getUsername().length() / 10 + '0';
		message += (_players[i]->getUsername().length() % 10 )+ '0';
		message += _players[i]->getUsername();
		message += _results.find(_players[i]->getUsername())->second / 10 + '0';
		message += _results.find(_players[i]->getUsername())->second % 10 + '0';
	}
	for (int i = 0; i < _players.size(); i++) {
		_players[i]->send(message);
		_players[i]->leaveGame();
		if (i < _players.size()) {
			_players[i]->setGame(nullptr);
		}
	}
}

void Game::sendFirstQuestion() {
	sendQuestionToAllUsers();
}

bool Game::handleNextTurn() {
	bool found = false;
	for (int i = 0; i < _players.size(); i++) {
		if (_players[i] != nullptr) {
			found = true;
		}
	}
	if (!found) {
		handleFinishGame();
		return false;
	}
	if (_currentTurnAnswers == _players.size()) {
		if (_currQuestionIndex == _questions.size() - 1) {
			handleFinishGame();
		}
		else {
			_currQuestionIndex++;
			sendQuestionToAllUsers();
		}
		_currentTurnAnswers = 0;
		return true;
	}
	return false;
}

bool Game::handleAnswerFromUser(User* user, int answerNo, int time) {
	bool isCorrect = false;
	_currentTurnAnswers++;
	if (answerNo == _questions[_currQuestionIndex]->getCorrectAnswerIndex()) {
		_results.find(user->getUsername())->second += 1;
		isCorrect = true;
	}
	string answer = "";
	if (answerNo != 5) {
		answer = _questions[_currQuestionIndex]->getAnswers()[answerNo - 1];
	}
	_db.addAnswerToPlayer(_id, user->getUsername(), _questions[_currQuestionIndex]->getId(),
		answer, isCorrect, time);
	user->send(isCorrect ? "1201" : "1200");
	handleNextTurn();
	if (_currQuestionIndex == _questions.size() - 1) {
		return false;
	}
	return true;
}

bool Game::leaveGame(User* currUser) {
	for (vector<User*>::iterator it = _players.begin(); it != _players.end(); it++) {
		if (*it ==currUser) {
			_players.erase(it);
			handleNextTurn();
			break;
		}
	}
	return (_players.size() != 0) && (_currQuestionIndex != _questions.size());
}
int Game::getID() {
	return _id;
}