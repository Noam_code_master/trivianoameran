#pragma once
#include <winsock2.h>
#include <windows.h>
#include <iostream>

#include "Game.h"
#include "Room.h"
class Room;
class Game;
class User{
public:
	User(std::string, SOCKET s);

	void send(std::string);
	
	std::string getUsername();
	SOCKET getSocket();
	
	Room* getRoom();
	Game * getGame();

	void setGame(Game * game);
	
	void clearRoom();
	bool createRoom(int, std::string, int, int, int);

	bool joinRoom(Room * room);
	void leaveRoom();
	
	int closeRoom();
	bool leaveGame();

	void clearGame();

private:

	std::string _username;
	
	Room * _currRoom;
	Game* _currGame;

	SOCKET _sock;
};