#pragma once
#include <vector>
#include <map>

#include "User.h"
#include "DataBase.h"
#include "Question.h"
using namespace std;
class User;
class DataBase;
class Game {
public:
	Game(const vector<User*> & users, int id, DataBase & db);
	~Game();

	void sendFirstQuestion();
	void handleFinishGame();

	bool handleNextTurn();
	bool handleAnswerFromUser(User *user, int, int);
	bool leaveGame(User* user);

	int getID();
private:

	/*bool insertGameToDB();
	void initQuestionsFromDB();*/
	void sendQuestionToAllUsers();

	vector<Question*> _questions;
	vector<User*> _players;

	int _question_no;
	int _currQuestionIndex;
	int _currentTurnAnswers;
	int _id;

	DataBase& _db;
	map<string, int> _results;

};