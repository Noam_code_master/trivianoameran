#pragma once
#include <iostream>
#include <vector>
using namespace std;

class Question {

public:
	Question(int id, string question, string correct_answer, string answer2, string answer3, string answer4);

	string getQuestion();
	string * getAnswers();

	int getCorrectAnswerIndex();
	int getId();

private:
	string _question;
	string _answers[4];

	int _correctAnswerIndex;
	int _id;

};