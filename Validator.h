#pragma once
#include <winsock2.h>
#include <windows.h>
#include <string>
#include "RecievedMessage.h"
#include "DataBase.h"
#include <map>
#include "User.h"

class Validator {
public:
	static bool isPasswordValid(string);
	static bool isUsernameValid(string);
};