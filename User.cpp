#include "User.h"
#include "Room.h"
#include "Game.h"

User::User(std::string name, SOCKET s){
	_username = name;
	_sock = s;
	_currRoom = nullptr;
	_currGame = nullptr;
}

void User::clearGame(){
	_currGame = nullptr;
}

std::string User::getUsername(){
	return _username;
}

SOCKET User::getSocket(){
	return _sock;
}

Room * User::getRoom(){
	return _currRoom;
}

Game * User::getGame(){
	return _currGame;
}

void User::setGame(Game * game){
	_currGame = game;
	_currRoom = nullptr;
}

bool User::joinRoom(Room * room){
	if (_currRoom != nullptr) {
		return false;
	}
	return _currRoom->joinRoom(this);
}

void User::leaveRoom(){
	if (_currRoom != nullptr) {
		_currRoom->leaveRoom(this);
	}
	_currRoom = nullptr;
}

int User::closeRoom(){
	if (_currRoom == nullptr) {
		return -1;
	}
	int result = _currRoom->closeRoom(this);
	if (result != -1) {
		delete _currRoom;
	}
	_currRoom = nullptr;
	return result;
}

bool User::leaveGame(){
	bool result = true;
	if (_currGame != nullptr) {
		result = _currGame->leaveGame(this);
	}
	_currGame = nullptr;
	return result;
}

bool User::createRoom(int id, std::string name, int maxUsers, int questionTime, int questionNo) {
	if (_currRoom != nullptr) {
		send("1141");
		return false;
	}
	_currRoom = new Room(id, this, name, maxUsers, questionTime, questionNo);
	send("1140");
	return true;
}
void User::clearRoom() {

 }

void User::send(string message) {
	Helper::sendData(_sock, message);
}