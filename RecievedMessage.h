#pragma once
#include <winsock2.h>
#include <windows.h>
#include <string>
#include <vector>
#include "User.h"
using namespace std;
class User;
class RecievedMessage {
private:
	SOCKET _sock;
	User* _user;
	int _messageCode;
	vector<string> _values;
public:
	RecievedMessage(SOCKET socket, int code) {
		_sock = socket;
		_messageCode = code;
		_values = vector<string>();
	}
	RecievedMessage(SOCKET socket, int code, vector<string> values) {
		_sock = socket;
		_messageCode = code;
		_values = values;
	}
	SOCKET getSock() { return _sock; }
	User* getUser() { return _user; }
	void setUser(User* user) { _user = user; }
	int getMessageCode() { return _messageCode; }
	vector<string>& getValues() { return _values; }

};