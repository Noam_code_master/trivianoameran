#include "Room.h"
#include <math.h>
#include <vector>

using namespace std;

Room::Room(int id, User* admin, string name, int maxUsers, int questionTime, int questionNo) {
	_id = id;
	_admin = admin;
	_maxUsers = maxUsers < 1 ? 1 : maxUsers;
	_questionTime = questionTime;
	_questionNo = questionNo;
	_name = name;
	_users.push_back(admin);
}

string Room::getUsersAsString(vector<User*> userList, User* excludeUser) {
	return "";
}

string Room::getUsersListMessage() {
	if (_admin->getGame() != nullptr || _admin->getRoom() != this) {
		return "1080";
	}
	string message = "108";
	message += _users.size() + '0';
	for (int i = 0; i < _users.size(); i++) {
		string username = _users[i]->getUsername();
		message += username.length() / 10 + '0';
		message += (username.length() % 10) + '0';
		message += username;
	}
	return message;
}

void Room::sendMessage(User* excludeUser, string message) {
	for (int i = 0; i < _users.size(); i++) {
		if (_users[i] != excludeUser) {
			_users[i]->send(message);
		}
	}
}

void Room::sendMessage(string message) {
	sendMessage(nullptr, message);
}

bool Room::joinRoom(User* user) {
	if (_users.size() == _maxUsers) {
		user->send("1101");
		return false;//Too much players already in room.
	}
	_users.push_back(user);
	string message = "1100";
	message += _questionNo / 10 + '0';
	message += (_questionNo % 10 )+ '0';
	message += _questionTime / 10 + '0';
	message += (_questionTime % 10 )+ '0';
	user->send(message);
	sendMessage(getUsersListMessage());
	return true;
}

void Room::leaveRoom(User* user) {
	if (user == nullptr) {
		return;
	}
	for (vector<User*>::iterator it = _users.begin(); it != _users.end(); it += 1) {
		if ((*it)->getUsername() == user->getUsername()) {
			_users.erase(it);
		}
	}
	user->send("1120");
	sendMessage(getUsersListMessage());
}

int Room::closeRoom(User* user) {
	if (user != _admin) {
		return -1;
	}
	for (int i = 0; i < _users.size(); i++) {
		_users[i]->send("116");
		if (_users[i] != _admin) {
			_users[i]->clearRoom();
		}
	}
	return _id;
}

vector<User*> Room::getUsers() {
	return _users;
}

int Room::getQuestionsNo() { return _questionNo; };
int Room::getId() { return _id; };
string Room::getName() { return _name; };